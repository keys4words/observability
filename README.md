# observability

## Infrastructure
- VM WSO2 - WSO2 + filebeat + vector (logshipper)
- VM Posgresql
- VM Monitoring - Prometheus + Grafana
- VM ELK - Elasticsearch + Logstash + Kibana
- LXC producer api - REST api on base FAST Api + cAdvicer:
    - REST api FAST API - personsfastapi 1pc.
    - REST api Golang - 10 pc.
- 2 VM with scripts (requests to API)

## Metrics
### WSO2
- jolokia osgi-agent + telegraf input jolokia2 plugin => prometheus + grafana
### Postgres 
- postgres_exporter => prometheus + grafana
### APIs (API - Applications)
myapione - myapione
myapitwo -myapitwo
three - three
four - four
five - four
six -  four
seven - five, six, seven
eight - eight - with 500 error support
nine - nine - with 500 error support
ten - ten - with 500 error support
personsfastapi - fastapi

## Logging
### WSO2 - vector logshipper + elasticsearch + kibana
- correlation logs - /{WSO2-home-dir}/correlation.log - logs with throughput ID
- access logs - /{WSO2-home-dir}/http_access_.2023-10-14.log
- audit logs - /{WSO2-home-dir}/audit.log

## Analytics
- apim_metrics - filebeat => ELK
dashboard:
    Traffic
    API analytics
    User analytics
    Errors
    Latency
    Cache
    Devices